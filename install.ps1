#!/usr/bin/env pwsh
# Install git-portable for SonPhat's product

Write-Host SonPhat"'"s GitPortable Installing...

function install_git($libDir, $gitDirName, $gitExe) {
    return "$libDir\$gitDirName\$gitExe";
}

$gitInstance = install_git "$home\.SonPhat" "MyGit" "git-cmd.exe";

Write-Host "install.ps1 > $gitInstance";

return $gitInstance;